# Flow explaination

```mermaid
graph TD
  RQ[Reader] -->|send messages| Q[Queue]
  Q --> |sends raw messages to worker| W[Worker]
  W --> |sends back processed messages| Q
  Q --> WQ[Writer]
  WQ --> |sends to IRC| I[IRC Server]
```

1. The Reader connects to the IRC Server and gathers messages from the channels we need
2. The Reader puts messages into a "unprocessed" queue named after the channel the message is from
3. The Worker will connect to the queues that he needs, or more likely to the queues named after the channels he manages. When the Worker is connected to the queue and fetched the informations about the channel from the APIs, he will start processing each message that is in the queue.
4. Once the message is processed, and, if a message needs to be sent back, he will enqueue the needed message to a queue named "outgoing-{botname}", i.e. if the bot is named mihoka, the queue will be named outoging-mihoka.
5. The Writer will connect to the queues related to his login (i.e. mihoka), and will fetch messages from that queue, to send them to the IRC server