import actions from "../utils/actions";

export interface ChannelGreetings {
    channel: string;
    uuid?: string;
    chatter_group?: string;
    chatter?: string;
    message: string;
    created_at?: string;
    updated_at?: string;
    deleted_at?: string;
}

export const Greetings = {
    clear: async (channel: string): Promise<boolean> => {
        return await actions.remove(`/channels/${channel}/greetings`);
    },
    create: async (data: ChannelGreetings): Promise<ChannelGreetings> => {
        return await actions.create<ChannelGreetings>(`/channels/${data.channel}/greetings`, data);
    },
    list: async (channel: string): Promise<ChannelGreetings[]> => {
        return await actions.list<ChannelGreetings>(`/channels/${channel}/greetings`);
    },
    patch: async (greeting: ChannelGreetings): Promise<ChannelGreetings> => {
        const { uuid, channel, message } = greeting;
        return await actions.patch<ChannelGreetings>(`/channels/${channel}/greetings/${uuid}`, { channel, message });
    },
    remove: async (greeting: string): Promise<boolean> => {
        return await actions.remove(`/greetings/${greeting}`);
    },
};

export default Greetings;
