import actions from "../utils/actions";

export interface ChatterActivity {
    id?: string;
    date?: string;
    login?: string;
    display?: string;
}

export interface UserInformations {
  id: string|undefined;
  login: string|undefined;
  display: string|undefined;
}

export const Activities = {
    get: async (channel: string, userId: string): Promise<ChatterActivity> => {
      return await actions.get(`/channels/${channel}/activities/${userId}`);
    },
    clear: async (channel: string): Promise<boolean> => {
        return await actions.remove(`/channels/${channel}/activities`);
    },
    list: async (channel: string): Promise<ChatterActivity[]> => {
        return await actions.list<ChatterActivity>(`/channels/${channel}/activities`);
    },
    update: async (channel: string, userId: string, informations?: ChatterActivity): Promise<ChatterActivity> => {
        return await actions.update<ChatterActivity>(`/channels/${channel}/activities/${userId}`, informations);
    },
    updateBulk: async (channel: string, activitiesList: ChatterActivity[]): Promise<ChatterActivity[]> => {
        return await actions.update(`/channels/${channel}/activities`, activitiesList);
    },
};

export default Activities;
