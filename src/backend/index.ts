import Activities from "./activities";
import Announcements from "./announcements";
import Client from "./backend";
import Greetings from "./greetings";
import Preferences from "./preferences";
import Quotes from "./quotes";
import Raffles from "./raffles";
import Translations from "./translations";

export { Activities, Announcements, Client, Greetings, Preferences, Quotes, Raffles, Translations };

export default {
  Activities,
  Announcements,
  Client,
  Greetings,
  Preferences,
  Quotes,
  Raffles,
  Translations,
};
