import actions from "../utils/actions";

export interface Chatter {
  id?: string;
  login: string;
  display?: string;
}

export const Channels = {
  get: async (uuid: string): Promise<Chatter> => {
    return await actions.get<Chatter>(`/channels/${uuid}`);
  },
  getByName: async (name: string): Promise<Chatter|null> => {
    const encodedQS = encodeURIComponent(JSON.stringify({ name }))
    const results = await actions.list<Chatter>(`/channels?where=${encodedQS}`);
    return results.length > 0 ? results[0] : null;
  },
  list: async (): Promise<Chatter[]> => {
    return await actions.list<Chatter>(`/channels`);
  },
  disable: async (uuid: string): Promise<boolean> => {
    return await actions.remove(`/channels/${uuid}/disable`);
  },
  update: async (uuid: string, channelData: Chatter): Promise<Chatter> => {
    return await actions.update<Chatter>(`/channels/${uuid}`, {
      id: channelData.id,
      login: channelData.login,
      display: channelData.display,
    });
  },
};

export default Channels;
