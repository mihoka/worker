import actions from "../utils/actions";

export interface FollowResponse {
  total: number;
  data: FollowRecord[];
}
export interface FollowRecord {
  from_id: string;
  from_name?: string;
  to_id: string;
  to_name?: string;
  followed_at?: string;
};
export interface UptimeResponse {
  data: StreamRecord[];
};
export interface StreamRecord {
  id: string;
  user_id: string;
  username: string;
  game_id: string;
  game_name: string;
  type: string;
  title: string;
  viewer_count: number;
  started_at: string;
  language: string;
  thumbnail_url: string;
};

export const Twitch = {
  follows: async (channel: string, user: string): Promise<FollowResponse> => {
    try {
      return await actions.get(`/twitch/follows?to=${channel}&from=${user}`);
    } catch (err) {
      return {
        total: 0,
        data: [],
      }
    }
  },
  uptime: async (channel: string): Promise<UptimeResponse> => {
    return await actions.get(`/twitch/uptime?from=${channel}`);
  },
}