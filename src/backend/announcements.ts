import actions from "../utils/actions";

export interface BackendAnnouncement {
  channel: string;
  sequence?: number;
  message: string;
  active: boolean;
  created_by: string;
  created_at?: string;
  updated_at?: string;
}

export const Announcements = {
  clear: async (channel: string): Promise<boolean> => {
    return await actions.remove(`/channels/${channel}/announcements`);
  },
  create: async (channel: string, announcement: BackendAnnouncement): Promise<BackendAnnouncement> => {
    return await actions.create<BackendAnnouncement>(`/channels/${channel}/announcements`, announcement);
  },
  get: async (channel: string, sequence: number): Promise<BackendAnnouncement> => {
    return await actions.get<BackendAnnouncement>(`/channels/${channel}/announcements/${sequence}`);
  },
  list: async (channel: string): Promise<BackendAnnouncement[]> => {
    return await actions.list<BackendAnnouncement>(`/channels/${channel}/announcements`);
  },
  patch: async (channel: string, sequence: number, announcement: BackendAnnouncement): Promise<BackendAnnouncement> => {
    return await actions.patch<BackendAnnouncement>(`/channels/${channel}/announcements/${sequence}`, announcement);
  },
  remove: async (channel: string, sequence: number): Promise<boolean> => {
    return await actions.remove(`/channels/${channel}/announcements/${sequence}`);
  },
};

export default Announcements;
