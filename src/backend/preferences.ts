import actions from "../utils/actions";

export interface BackendPreference {
    uuid?: string;

    channel?: string;
    key?: string|null;
    value: string|null;
}

export const Preferences = {
    clear: async (channel: string): Promise<boolean> => {
        return await actions.remove(`/channels/${channel}/preferences`);
    },
    create: async (preferenceData: BackendPreference): Promise<BackendPreference> => {
        const { channel } = preferenceData;
        Reflect.deleteProperty(preferenceData, "channel");

        return await actions.create<BackendPreference>(`/channels/${channel}/preferences`, preferenceData);
    },
    get: async (uuid: string): Promise<BackendPreference> => {
        return await actions.get<BackendPreference>(`/preferences/${uuid}`);
    },
    list: async (channel: string): Promise<BackendPreference[]> => {
        return await actions.list<BackendPreference>(`/channels/${channel}/preferences`);
    },
    remove: async (uuid: string): Promise<boolean> => {
        return await actions.remove(`/preferences/${uuid}`);
    },
    update: async (preferenceData: BackendPreference): Promise<BackendPreference> => {
        return await actions.update<BackendPreference>(`/preferences/${preferenceData.uuid}/preferences`, {
            value: preferenceData.value,
        });
    },
};

export default Preferences;
