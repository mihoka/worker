import { Client } from "@hapi/nes";
import { Logger } from "../logger";
import { Sentry } from "../sentry";
export const singleton = new Client(process.env.MIHOKA_ENDPOINT || "ws://localhost:8080");
singleton.onDisconnect = (willReconnect: boolean, logData): void => {
    Logger.warn({
        willReconnect,
        ...logData,
    }, `Lost connection with Backend.`);
};

singleton.onConnect = (): void => {
    Logger.info(`Backend is now connected.`);
};

export default singleton;
