import actions from "../utils/actions";

export interface Language {
  language_id: string;
  language_display: string;
  created_at: string;
  updated_at?: string;
}

export interface Translation {
  key: string | null;
  language?: string;
  channel?: string;
  value: string | null;
  created_at: string;
  updated_at?: string;
}

export const Translations = {
  Channels: {
    create: async (channel: string, translation: Translation): Promise<Translation> => {
      return await actions.create<Translation>(`/channels/${channel}/translations`, translation);
    },
    clear: async (channel: string): Promise<boolean> => {
      return await actions.remove(`/channels/${channel}/translations`);
    },
    remove: async (channel: string, key: string): Promise<boolean> => {
      return await actions.remove(`/channels/${channel}/translations/${key}`);
    },
    list: async (channel: string): Promise<Map<string, string>> => {
      const translations = await actions.list<Translation>(`/channels/${channel}/translations`);
      const translationMap = new Map<string, string>();
      for (const { key, value } of translations) {
        translationMap.set(key, value);
      }
      return translationMap;
    },
    get: async (channel: string, key: string): Promise<Translation> => {
      return await actions.get<Translation>(`/channels/${channel}/translations/${key}`);
    },
    patch: async (channel: string, key: string, translation: Translation): Promise<Translation> => {
      delete translation.key;
      return await actions.patch<Translation>(`/channels/${channel}/translations/${key}`, translation);
    },
  },
  Globals: {
    getLanguages: async (): Promise<Language[]> => {
      return await actions.list<Language>("/translations");
    },
    getTranslations: async (language: string): Promise<Map<string, string>> => {
      const translations = await actions.list<Translation>(`/translations/${language}`);
      const translationMap = new Map<string, string>();
      translations.forEach(
        (translation: any) => translationMap.set(translation.key, translation.value),
      );
      return translationMap;
    },
  },
};

export default Translations;
