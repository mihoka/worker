import actions from "../utils/actions";

export interface BackendQuote {
    channel: string;
    sequence?: number;

    message: string;
    game?: string;

    created_at?: string;
    created_by?: string;
    updated_at?: string;
    deleted_at?: string;
}

export const Quotes = {
    clear: async (channel: string) => {
        return await actions.remove(`/channels/${channel}/quotes`);
    },
    create: async (quote: BackendQuote) => {
        return await actions.create(`/channels/${quote.channel}/quotes`, {
            created_by: quote.created_by,
            game: quote.game,
            message: quote.message,
        });
    },
    get: async (channel: string, sequence: number) => {
        return await actions.list(`/channels/${channel}/quotes/${sequence}`);
    },
    list: async (channel: string) => {
        return await actions.list(`/channels/${channel}/quotes`);
    },
    patch: async (quote: BackendQuote) => {
        const allowedKeys = ["game", "created_by", "message"];
        const payload = {};
        for (const key of allowedKeys) {
            if (Reflect.has(quote, key)) {
                Reflect.set(payload, key, Reflect.get(quote, key));
            }
        }
        return await actions.patch(`/channels/${quote.channel}/quotes/${quote.sequence}`, payload);
    },
    remove: async (channel: string, sequence: number) => {
        return await actions.remove(`/channels/${channel}/quotes/${sequence}`);
    },
};

export default Quotes;
