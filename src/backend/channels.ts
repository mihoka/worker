import actions from "../utils/actions";

export interface MihokaChannel {
  uuid?: string;
  id: string;
  name: string;
  display: string;
  kind?: string;
  is_active?: boolean;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export const Channels = {
  get: async (uuid: string): Promise<MihokaChannel> => {
    return await actions.get<MihokaChannel>(`/channels/${uuid}`);
  },
  getByName: async (name: string): Promise<MihokaChannel|null> => {
    const encodedQS = encodeURIComponent(JSON.stringify({ name }))
    const results = await actions.list<MihokaChannel>(`/channels?where=${encodedQS}`);
    return results.length > 0 ? results[0] : null;
  },
  list: async (): Promise<MihokaChannel[]> => {
    return await actions.list<MihokaChannel>(`/channels`);
  },
  disable: async (uuid: string): Promise<boolean> => {
    return await actions.remove(`/channels/${uuid}/disable`);
  },
  update: async (channelData: MihokaChannel): Promise<MihokaChannel> => {
    return await actions.update<MihokaChannel>(`/channels/${channelData.uuid}`, {
      id: channelData.id,
      name: channelData.name,
      display: channelData.display,
    });
  },
};

export default Channels;
