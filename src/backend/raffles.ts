import actions from "../utils/actions";

// TODO write file
export interface BackendRaffle {
    uuid: string;
}

export interface BackendRaffleMember {
    raffle: string;
}

export const RaffleMembers = {
    create: async (raffleId: string, member: BackendRaffleMember) => {
        //
    },
    list: async (raffleId: string) => {
        //
    },
    remove: async (raffleId: string, memberId: string) => {
        //
    },
};

export const Raffles = {
    clear: async (channelId: string) => {
        return await actions.remove(`/channels/${channelId}/raffles`);
    },
    create: async (channelId: string, raffle: BackendRaffle) => {
        //
    },
    draw: async (raffleId: string) => {
        //
    },
    get: async (raffleId: string) => {
        return await actions.get(`/raffles/${raffleId}`);
    },
    getCurrent: async (channelId: string) => {
        //
    },
    list: async (channelId: string) => {
        //
    },
    remove: async (raffleId: string) => {
        return await actions.remove(`/raffles/${raffleId}`);
    },
};

export default Raffles;
