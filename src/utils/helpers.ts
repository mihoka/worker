import { IRC_TYPES } from "twsc";
import { UserInformations } from "../backend/activities";

export interface ParsedMessage {
  content: string;
  params: string[];
  prefix: string[][];
  raw: string;
  tags: string[][];
  type: IRC_TYPES;
}
export const isAuthorized = (statusCode: number): boolean => {
    if (statusCode === 401) {
      throw Error("Authentication required");
    }
    if (statusCode === 403) {
      throw Error("Access forbidden");
    }
    return true;
};
export const isImmuneToCooldown = (message: ParsedMessage): boolean => {
  if ( !message.tags) {
    return false;
  }
  const badgesRecord = message.tags.find(tag => tag[0] === "badges");
  if (!badgesRecord) {
    return false;
  }
  return badgesRecord[1].split(",").map(badge => badge.replace(/\/.+/g, "")).some(badge => ["moderator", "vip", "staff", "broadcaster"].includes(badge));
}

export const extractUserInformations = (message: ParsedMessage): UserInformations  => {
  if (!message.tags) {
    return;
  }
  const userIdTag = message.tags.find(tag => tag[0] === "user-id");
  const userDisplayTag = message.tags.find(tag => tag[0] === "display-name");
  const userLoginPrefix = message.prefix.find(prefix => prefix[0] === "user");
  return {
    id: userIdTag && userIdTag.length > 1 && userIdTag[1] || undefined,
    login: userLoginPrefix && userLoginPrefix.length > 1 && userLoginPrefix[1] || undefined,
    display: userDisplayTag && userDisplayTag.length > 1 && userDisplayTag[1] || undefined,
  };
}

export const timeSinceDate = (dateString: string, sentAt: string): number => {
  const date = new Date(dateString);
  const now = new Date(sentAt);
  return now.valueOf() - date.valueOf();
}

export const getMapValOrDefault = <K,V>(map: Map<K, V>, key: K, fallback: V): V => {
  return map.has(key) ? map.get(key) : fallback;
}
export const parseMessageDate = (message: ParsedMessage): string => {
  // fetch date of message on IRC
  const messageDateRaw = (
    message.tags.find(tag => tag[0] === "tmi-sent-ts")
    || [null, String(Date.now())]
  ).pop();
  // convert tag to a ISODate
  return new Date(parseInt(messageDateRaw, 10)).toISOString();
}
export const isUserOnCooldown = (userActivityStamp: number | undefined, channelCooldown: number, message: ParsedMessage): boolean => {
  if (!userActivityStamp) {
    return false;
  }
  // return value
  return !isImmuneToCooldown(message) && userActivityStamp < channelCooldown;
}