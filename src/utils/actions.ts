import { singleton as Backend } from "../backend/backend";
import { isAuthorized } from "./helpers";

export const get = async <T>(endpoint: string): Promise<T> => {
  const method = "GET";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
  });

  isAuthorized(backendResponse.statusCode);

  if (backendResponse.statusCode !== 200) {
    throw Error("Invalid status code");
  }
  return backendResponse.payload;
};

export const list = async <T>(endpoint: string): Promise<T[]> => {
  const method = "GET";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
  });

  isAuthorized(backendResponse);

  if (backendResponse.statusCode !== 200) {
    throw Error("Invalid status code");
  }
  return backendResponse.payload.data;
};

export const create = async <T>(endpoint: string, payload: T): Promise<T> => {
  const method = "POST";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
    payload,
  });

  isAuthorized(backendResponse);

  if (backendResponse.statusCode !== 201) {
    throw Error("Invalid status code");
  }

  return backendResponse.payload;
};

export const update = async <T>(endpoint: string, payload?: T): Promise<T> => {
  const method = "PUT";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
    payload,
  });

  isAuthorized(backendResponse);

  if (![201, 200].includes(backendResponse.statusCode)) {
    throw Error("Invalid status code");
  }

  return backendResponse.payload;
};

export const patch = async <T>(endpoint: string, payload: T): Promise<T> => {
  const method = "PATCH";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
    payload,
  });

  isAuthorized(backendResponse);

  if (backendResponse.statusCode !== 200) {
    throw Error("Invalid status code");
  }

  return backendResponse.payload;
};

export const remove = async (endpoint: string): Promise<boolean> => {
  const method = "DELETE";
  const backendResponse = await Backend.request({
    method,
    path: endpoint,
  });

  isAuthorized(backendResponse);

  if (backendResponse.statusCode !== 204) {
    throw Error("Invalid status code");
  }

  return true;
};

export const actions = {
  create,
  get,
  list,
  patch,
  remove,
  update,
};

export default actions;
