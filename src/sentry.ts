declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}
global.__rootdir__ = __dirname || process.cwd();
import { RewriteFrames } from '@sentry/integrations';
import * as Sentry from "@sentry/node";
Sentry.init({
    dsn: process.env.WORKER_SENTRY_DSN,
    integrations: [new RewriteFrames({
        root: global.__rootdir__
    })],
    attachStacktrace: true,
    environment: process.env.NODE_ENV,
    release: process.env.RELEASE_HASH || 'local-dev',
});
export { Sentry };