import { Sentry } from "./sentry";
import { queueWorker } from "./worker/worker";
import { Logger } from "./logger/logger";
import { singleton as translations } from "./translations/translations";
import { singleton as backend } from "./backend/backend";

process.on("uncaughtException", (error) => {
  Logger.error(error, "Uncaught exception detected");
  Sentry.captureException(error);
});

process.on("unhandledRejection", (reason) => {
  Sentry.captureEvent({
    level: Sentry.Severity.Warning,
    message: 'Unhandled rejection from a promise',
    extra: {
      reason,
    },
  });
  Sentry.captureMessage("Unhandled rejection");
});

(async (): Promise<void> => {
  try {
    await backend.connect({
      auth: {
        headers: {
          'x-access-token': process.env.API_TOKEN,
        }
      }
    });
  } catch (err) {
    Logger.fatal(err, "Failed to connect to the server");
    process.exit(1);
  }
  await translations.initialize();
  await queueWorker();
})();