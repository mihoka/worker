import { Command, CommandType, validateAuthorization, Authorization } from "./command";
import { Greetings, Preferences } from "../backend";
import { WorkerChannel } from "../worker/channel";

export const updateMode = async (mode: number, channel: WorkerChannel) => {
    if (isNaN(mode) || ![0, 1, 2].includes(mode)) {
        return;
    }
    const modePreferenceKey = 'greetings.welcome.mode';
    await Preferences.create({
        channel: channel.informations.uuid,
        key: modePreferenceKey,
        value: JSON.stringify({value:mode}),
    });
    return true;
}

export const welcomeCommands: Command = {
    Type: CommandType.Command,
    isMatching: (message): boolean => {
        if (!validateAuthorization(message, Authorization.Moderator)) {
            return false;
        }
        if (!message.content.toLocaleLowerCase().startsWith('!welcome')) {
            return false;
        }
        const validSubCommands = ['mode', 'message', 'messageFor'];
        return validSubCommands.some(v => message.content.toLocaleLowerCase().startsWith(`!welcome ${v} `));
    },
    execute: async (message, channel, user): Promise<boolean>  => {
        if (!channel.preferences.get('greetings.enabled')) {
            // Greeting commands aren't enabled, skipping.
            return;
        }

        const subCommand = message.content.substr('!welcome'.length +1).trim().split(' ').shift().toLowerCase();
        const params = message.content.trim().split(' ').slice(2);

        if (subCommand === 'mode') {
            if (!params.length) {
                return;
            }
            const mode = parseInt(params[0], 10);
            return await updateMode(mode, channel);
        } else if (subCommand === 'message') {
            const message = params.join(' ');
            await Greetings.create({
                channel: channel.informations.uuid,
                message,
            });
            return true;
        } else if (subCommand === 'messageFor') {
            const targetUsername = params[0];
            //
            const message = params.slice(1).join(' ');
            await Greetings.create({
                channel: channel.informations.uuid,
                chatter: "0", // TODO check chatters, find via API if not
                message,
            });
            return true;
        }
        return false;
    },
    isOnCooldown: (message, channel) => false,
};
