import { StreamRecord, Twitch, UptimeResponse } from "../backend/twitch";
import { enqueueMessage } from "../worker/worker-actions";
import { Command, CommandType, defaultNativeCalledAt, isOnCooldown } from "./command";

export const uptime: Command = {
  called: defaultNativeCalledAt,
  delay: 30000,
  type: CommandType.Command,
  isOnCooldown: undefined,
  isMatching: (message) => message.content.trim() === "!uptime",
  execute: async (_, channel, user) => {
    // const channelLanguage = channel.preferences.get('language') || 'en';
    let uptimeQuery: UptimeResponse|undefined;
    try {
      uptimeQuery = await Twitch.uptime(channel.informations.id);
      if (!uptimeQuery || !uptimeQuery.data || uptimeQuery.data.length < 1) {
        return;
      }
      const streamData: StreamRecord = uptimeQuery.data[0];
      const started = new Date(streamData.started_at).valueOf();
      const current = new Date().valueOf();
      const diff = current - started;

      if (diff < 1000) {
        throw Error("Invalid time difference, default to offline");
      }

      const duration = {
        seconds: Math.floor(diff % 60000 / 1000),
        minutes: Math.floor(diff / 60000) % 60,
        hours: Math.floor(diff / 3600000) % 24,
        days: Math.floor(diff / 86400000),
      };
      let durationData= [];
      if (duration.days > 0) 
        durationData.push(`${duration.days} jour(s)`);
      if (duration.hours > 0) 
        durationData.push(`${duration.hours} heure(s)`);
      if (duration.minutes > 0) 
        durationData.push(`${duration.minutes} minute(s)`);
      if (duration.seconds > 0) 
        durationData.push(`${duration.seconds} seconde(s)`);
      enqueueMessage("mihoka", channel.informations.name, `${user.display}: Le stream est en direct depuis ${durationData.join(", ")}`);
    } catch (err) {
      enqueueMessage("mihoka", channel.informations.name, `@${user.display}: The stream is either offline or there was an issue fetching the informations.`);
      return;
    }
  }
}
uptime.isOnCooldown = isOnCooldown(uptime);