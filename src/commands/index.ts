import { Command } from "./command";
import { followAge } from "./followage";
import { uptime } from "./uptime";

export const natives: Command[] = [
  followAge,
  uptime,
];

export default natives;