import { Twitch } from "../backend/twitch";
import { enqueueMessage } from "../worker/worker-actions";
import { Command, CommandType, defaultNativeCalledAt, isOnCooldown } from "./command";

export const followAge: Command = {
  called: defaultNativeCalledAt,
  delay: 5000,
  type: CommandType.Command,
  isOnCooldown: undefined,
  isMatching: (message) => message.content.trim().startsWith("!fc"),
  execute: async (_, channel, user) => {
    const channelLanguage = channel.preferences.get('language') || 'en';
    if (user.id === channel.informations.id) {
      // broadcaster cannot request FC command
      return;
    }
    const follow = await Twitch.follows(channel.informations.id, user.id);
    if (follow.total === 0) {
      enqueueMessage("mihoka", channel.informations.name, `${user.display} is not following the channel`);
      return;
    }
    // user is following, preparing format
    const dateTimeFormat = new Intl.DateTimeFormat(channelLanguage, {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
    // extraction record
    const record = follow.data[0];
    const sinceDate = new Date(record.followed_at);
    const currDate = new Date();
    // starting format of dates
    const diff = {
      y: currDate.getUTCFullYear() - sinceDate.getUTCFullYear(),
      m: currDate.getUTCMonth() - sinceDate.getUTCMonth(),
      d: currDate.getUTCDate() - sinceDate.getUTCDate(),
    };
    diff.m = diff.d < 0 ? diff.m - 1 : diff.m;
    diff.d = diff.d < 0 ? new Date(sinceDate.getUTCFullYear(), sinceDate.getUTCMonth(), 0).getUTCDate() + diff.d : diff.d;
    // send message
    enqueueMessage(
      "mihoka", channel.informations.name, `${user.display}, vous suivez la chaîne depuis le ${dateTimeFormat.format(sinceDate)
      } (${diff.y > 0 ? `${diff.y} année(s), ` : ''
      }${diff.m > 0 ? `${diff.m} mois, ` : ''
      }${diff.d} jour(s))`
    );
    return;
  }
}
followAge.isOnCooldown = isOnCooldown(followAge);