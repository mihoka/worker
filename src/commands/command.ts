import { isImmuneToCooldown, ParsedMessage, parseMessageDate, timeSinceDate } from "../utils/helpers";
import { WorkerChannel } from "../worker/channel";
import { UserInformations } from "../backend/activities";

export enum CommandType {
    Trigger = "trigger",
    Command = "command",
};
export enum CommandCategory {
    Text = "text",
    Native = "native",
    Script = "script",
};
export enum Authorization {
    Support = "support",
    Broadcaster = "broadccaster",
    Moderator = "moderator",
    VIP = "vip",
    Viewer = "viewer",
};
export const validateAuthorization = (message: ParsedMessage, requiredAuthorization: Authorization): boolean => {
    if (requiredAuthorization === Authorization.Viewer) return true;
    const badges: string[] = message && message.tags && message.tags.find(t => t[0] === 'badges') || [];
    const badgeList = badges && badges.length > 1 && badges[1].split(',') || [];
    const isVIP = badgeList.includes('vip/1');
    const isMod = badgeList.includes('moderator/1');
    const isBroadcaster = badgeList.includes('broadcaster/1');
    // support users of mihoka, TODO fetch from APIs
    const supportIds = ["51175553"];
    const userIdTag = message && message.tags && message.tags.find(t => t[0] === "user-id");
    const userId = userIdTag && userIdTag.length > 1 && userIdTag[1];
    const isSupport = supportIds.includes(userId);


    if (requiredAuthorization === Authorization.VIP) {
        return isVIP || isMod || isBroadcaster || isSupport;
    } else if (requiredAuthorization === Authorization.Moderator) {
        return isMod || isBroadcaster || isSupport;
    } else if (requiredAuthorization === Authorization.Broadcaster) {
        return isBroadcaster || isSupport;
    }
    return false;
};
export interface Command {
    type: CommandType;
    delay: number;
    called: string;
    isMatching: (message: ParsedMessage) => boolean;
    isOnCooldown: (message: ParsedMessage, channel: WorkerChannel) => boolean;
    execute: (message: ParsedMessage, channel: WorkerChannel, user: UserInformations) => Promise<void>;
};
export interface CommandRecord {
    uuid: string;
    category: CommandCategory;
    type: CommandType;
    channel: string;

    input: string;
    output: string;

    delay: number;
    called: number;
    hidden: boolean;
};
export interface CustomCommand extends Command {
    record: CommandRecord;
    setCalled: () => Promise<void>;
};
export const isOnCooldown = (command: Command) => {
    return (
        message: ParsedMessage,
        _: WorkerChannel
    ) => {
        if (isImmuneToCooldown(message)) return false;
        const messageDate = parseMessageDate(message);
        const timeDiff = timeSinceDate(command.called, messageDate);
        return timeDiff < command.delay;
    };
}
export const defaultNativeCalledAt = "1970-01-01T00:00:00.000Z";