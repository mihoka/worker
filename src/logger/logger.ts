import { createLogger } from "bunyan";

export const Logger = createLogger({
    environment: process.env.NODE_ENV,
    level: "debug",
    name: "worker",
    stream: process.stdout,
});
export default Logger;
