import { ChatterActivity, UserInformations, Activities } from '../backend/activities';
import { ChannelGreetings } from '../backend/greetings';
import { singleton as translations } from '../translations/translations';
import { getMapValOrDefault, ParsedMessage } from '../utils/helpers';
import { WorkerChannel } from './channel';

export const getGreetingFor = (channel: WorkerChannel, user: string): string | null => {
  let welcomeMessage: ChannelGreetings | undefined;
  // verify that the user has a custom welcome
  welcomeMessage = channel.greetings.find(greeting => (greeting.chatter === user));
  if (welcomeMessage) {
    return welcomeMessage.message;
  }
  // verify that the channel has a custom welcome
  welcomeMessage = channel.greetings.find(greeting => (greeting.chatter === ''));
  if (welcomeMessage) {
    return welcomeMessage.message;
  }
  // if not, use default of channel language
  const channelLanguage = getMapValOrDefault(channel.preferences, `language`, 'en');
  return translations.get(channelLanguage, 'chat.welcome.message');
}
export const replacePlaceholders = (message: string, channel: WorkerChannel, user: UserInformations, ircMessage?: ParsedMessage): string => {
  const contentArray = ircMessage && ircMessage.content && ircMessage.content.split(' ');
  // TODO: add guards and handling if message is not of type IRC_TYPES.PRIVMSG
  const [, target = user.display] = contentArray;
  return [
    [/\$\(USER\)/gi, user.login],
    [/\$\(USERDISPLAY\)/gi, user.display],
    [/\$\(USERID\)/gi, user.id],
    [/\$\(CHANNEL\)/gi, channel.informations.name],
    [/\$\(TARGET\)/gi, target],
  ].reduce((acc, [key, value]) => {
    return acc.replace(key, value as string);
  }, message);
}
export const pushActivities = async (channel: WorkerChannel): Promise<void> => {
  const updatedActivities: ChatterActivity[] = [];
  channel.updatedActivities.forEach(
    id => updatedActivities.push(
      channel.activities.get(id)
    )
  );
  await Activities.updateBulk(channel.informations.uuid, updatedActivities);
}