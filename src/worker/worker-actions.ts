import { connect, ChannelWrapper } from "amqp-connection-manager";
import Logger from "../logger";
import { ParsedMessage } from "../utils/helpers";

export const parseMessageDate = (message: ParsedMessage): string => {
  // fetch date of message on IRC
  const messageDateRaw = message.tags.find(tag => tag[0] === "tmi-sent-ts");
  // convert tag to a ISODate
  return (
    new Date(messageDateRaw && messageDateRaw.length > 1 ? Number(messageDateRaw[1]) : Date.now()).toISOString()
  );
};
export const createAMQPChannel = (): ChannelWrapper => {
  const server = connect([process.env.AMQP_ADDRESS || 'amqp://localhost:5672']);
  try {
    return server.createChannel();
  } catch (err) {
    Logger.fatal(err, "Failed to connect to the AMQP server");
    process.exit(1);
  }
};
export const enqueueMessage = async (botId: string, channel: string, message: string) => {
  Logger.warn({botId, message}, "Pretended to enqueue a message but no destination server is set yet");
};
