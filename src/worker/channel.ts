import { Channels, MihokaChannel } from "../backend/channels";
import { Preferences, Greetings } from "../backend";
import { Activities, ChatterActivity } from "../backend/activities";
import { ChannelGreetings } from "../backend/greetings";

import { Sentry } from "../sentry";
import Logger from "../logger";

/**
 * A channel managed by the worker
 */
export interface WorkerChannel {
  preferences: Map<string, string>;
  greetings: ChannelGreetings[];
  informations: MihokaChannel;
  activities: Map<string, ChatterActivity>;
  updatedActivities: string[];
};

/**
 * Claims authority against a channel, as a worker. This prevents the main server to
 * re-allocate the channel to another worker, as we confirmed we will take care of
 * that one.
 * @param uuid The channel UUID to claim
 * @todo implement in the backend and here
 */
export const claimChannel = async (uuid: string) => {
  //
}
/**
 * Releases the authority we claimed earlier on a channel. Usually, it means the channel has been without content for more than a few hours and is now leaving the processing mode
 * @param uuid the channel UUID to release from usage
 * @todo implement in the backend and here
 */
export const releaseChannel = async (uuid: string) => {
  //
}

/**
 * fetches and creates a WorkerChannel object for the worker to manage. This channel object
 * holds informations about the channel, allowing the worker to easily process messages without
 * having to do multiple queries to the  APIs each time a message for the same channel is
 * received.
 * @param uuid the channel UUID to fetch informations about
 */
export const fetchChannel = async (uuid: string): Promise<WorkerChannel | null> => {
  const informations = await fetchChannelInformations(uuid);
  if (!informations) {
    return null;
  }
  const preferences = await fetchChannelPreferences(uuid);
  const greetings = await fetchChannelGreetings(uuid);
  const activities = await fetchChannelActivities(uuid);
  return {
    greetings,
    preferences,
    informations,
    activities,
    updatedActivities: [],
  };
}
export const fetchChannelInformations = async (channelUUID: string): Promise<MihokaChannel | null> => {
  try {
    return await Channels.get(channelUUID);
  } catch (err) {
    Logger.error(err, "failed to fetch the channel informations");
    Sentry.captureException(err);
  }
  return null;
}
export const fetchChannelPreferences = async (channelUUID: string): Promise<Map<string, string>> => {
  try {
    const preferences = await Preferences.list(channelUUID);
    return new Map<string, string>(preferences.map(pref => ([pref.key, pref.value])));
  } catch (err) {
    Logger.error(err, "failed to fetch the channel preferences");
    Sentry.captureException(err);
    return new Map<string, string>();
  }
}
export const fetchChannelGreetings = async (channelUUID: string): Promise<ChannelGreetings[]> => {
  try {
    return await Greetings.list(channelUUID);
  } catch (err) {
    Logger.error(err, "failed to fetch the channel greetings");
    Sentry.captureException(err);
    return [];
  }
}
export const fetchChannelActivities = async (channelUUID: string): Promise<Map<string, ChatterActivity>> => {
  try {
    const activities = await Activities.list(channelUUID);
    return new Map<string, ChatterActivity>(activities.map(activity => ([activity.id, activity])));
  } catch (err) {
    Logger.error(err, "failed to fetch channel activities");
    Sentry.captureException(err);
    return new Map<string, ChatterActivity>();
  }
}
export const findChannelId = async (channelName: string): Promise<string> => {
  try {
    return (await Channels.getByName(channelName)).uuid;
  } catch (err) {
    Logger.error(err, "failed to find the channel ID based on its name.");
    Sentry.captureException(err);
    return null;
  }
}
