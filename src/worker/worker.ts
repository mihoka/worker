import { UserInformations } from "../backend/activities";
import { Channels } from "../backend/channels";
import {
  isImmuneToCooldown,
  extractUserInformations,
  getMapValOrDefault,
  ParsedMessage,
  timeSinceDate,
} from "../utils/helpers";
import Logger from "../logger";
import { Sentry } from "../sentry";
import {
  fetchChannel,
  WorkerChannel,
} from "./channel";
import {
  replacePlaceholders,
  getGreetingFor,
  pushActivities,
} from "./channel-actions";
import {
  enqueueMessage,
  createAMQPChannel,
} from "../worker/worker-actions";
import { ChannelWrapper } from "amqp-connection-manager";
import { Message } from "amqplib";

import { natives } from "../commands";
import { IRC_TYPES } from "twsc";

export const managedChannels = new Map<string, WorkerChannel>();
export const channelName2id = new Map<string, string>();
export let amqpChannel: ChannelWrapper|undefined;

export const isUserOnCooldown = (userActivityStamp: number | undefined, channelCooldown: number, message: ParsedMessage): boolean => {
  if (!userActivityStamp) {
    return false;
  }
  return !isImmuneToCooldown(message) && userActivityStamp > -1 && userActivityStamp < channelCooldown;
}

export const updateChannelUserActivity = (
  channel: WorkerChannel, 
  userDetails: UserInformations, 
  timestamp: string
): void => {
  channel.updatedActivities.push(userDetails.id);
  // make array unique, to avoid multiple times the same user id in array
  channel.updatedActivities = [ ...new Set(channel.updatedActivities)];
  channel.activities.set(userDetails.id, {
    date: timestamp,
    id: userDetails.id,
    login: userDetails.login,
    display: userDetails.display,
  });
}

export const processQueueMessage = async (message: Message) => {
  // parse back the JSON from message into an object
  const actualMessage = JSON.parse(message.content.toString()) as ParsedMessage;
  if (actualMessage.type !== IRC_TYPES.PRIVMSG) {
    amqpChannel.ack(message);
  }
  const sentAt = new Date(parseInt((actualMessage.tags.find((tag) => tag[0] === "tmi-sent-ts") || ["", ""])[1], 10)).toISOString();
  // store the user informations, if any
  const userInformations = extractUserInformations(actualMessage);
  // Gather informations about channel via our APIs
  const foundId = channelName2id.get(actualMessage.params[0].substr(1));
  const foundChannel = managedChannels.get(foundId);
  // We know the channel exists, is active and has a uuid.

  // Checking cooldowns for welcome message and the time between two messages
  const channelCooldown = parseInt(getMapValOrDefault(foundChannel.preferences, 'greetings.messages.cooldown', '5000'), 10);
  const channelWelcomeDelay = parseInt(getMapValOrDefault(foundChannel.preferences, 'greetings.welcome.delay', '18000000'), 10);
  const channelWelcomeMode = parseInt(getMapValOrDefault(foundChannel.preferences, 'greetings.welcome.mode', '0'), 10);

  // Verify user cooldown, drop if on cooldown
  const userActivityDetails = foundChannel.activities.get(userInformations.id);

  const userCooldown = userActivityDetails && userActivityDetails.date ? timeSinceDate(userActivityDetails.date, sentAt) : null; 

  if (isUserOnCooldown(
    userCooldown,
    channelCooldown,
    actualMessage
  )) {
    Logger.warn(userInformations, 'User is on cooldown, dropping');
    amqpChannel.ack(message);
    return;
  }

  // Update user activity on this channel with newly fetched data
  updateChannelUserActivity(foundChannel, userInformations, sentAt);

  // In case the user cooldown is bigger than the welcome delay, welcome him again
  if ([1, 2].includes(channelWelcomeMode) && (!userCooldown || userCooldown > channelWelcomeDelay)) {
    const welcomeMessage = replacePlaceholders(
      getGreetingFor(foundChannel, userInformations.id),
      foundChannel,
      userInformations,
      actualMessage
    );
    Logger.debug({ user: userInformations.login, message: welcomeMessage }, "Welcoming user into the stream");
    enqueueMessage("mihoka", foundChannel.informations.uuid, welcomeMessage);
  }
  // 2 - verify that the message is a command
  Logger.debug({
    count: natives.length
  }, "number of current native commands");
  for (const command of natives) {
    Logger.debug({command}, "the command we are using currently");
    if (command.isMatching(actualMessage) && !command.isOnCooldown(actualMessage, foundChannel)) {
      command.execute(actualMessage, foundChannel, userInformations);
      command.called = new Date().toISOString();
    }
  }
  // 2.1 - verify permissions for said command
  // 3 - verify announcements criterias
  pushActivities(foundChannel);
  amqpChannel.ack(message);
}

export const queueWorker = async (): Promise<void> => {
  Logger.info("Starting worker");
  amqpChannel = createAMQPChannel();
  const channelList = await Channels.list();
  Logger.info(channelList, "obtained channels");
  for (const mihokaChannel of channelList) {
    Logger.info(mihokaChannel, "processing channel");
    try {
      // fetch channel informations
      managedChannels.set(mihokaChannel.uuid, await fetchChannel(mihokaChannel.uuid));
      channelName2id.set(mihokaChannel.name, mihokaChannel.uuid);
      amqpChannel.addSetup(async (actualAmqpChannel) => {
        await actualAmqpChannel.assertQueue(mihokaChannel.name);
        actualAmqpChannel.consume(mihokaChannel.name, processQueueMessage);
      })
      Logger.info(
        {uuid: mihokaChannel.uuid, name: mihokaChannel.name},
        "Channel has been fetched, joining the queue for it"
      );
    } catch (err) {
      Logger.error(err, "Failed to fetch channel details");
      Sentry.captureException(err);
    }
  }
};

export default queueWorker;
