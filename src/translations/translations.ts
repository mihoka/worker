import { Client } from "../backend";
import { Language, Translation, Translations } from "../backend/translations";

import mainLogger from "../logger";
const Logger = mainLogger.child({
    component: "translations",
});

export class TranslationsManager {
    private defaultLocale = "en";
    private languages: Map<string, Map<string, string>>;

    constructor() {
        this.languages = new Map<string, Map<string, string>>();
    }
    public getDefaultLocale(): string {
        return this.defaultLocale;
    }
    public async initialize(): Promise<void> {
        // Fetch all the languages we have in the database
        const availableLanguages = await Translations.Globals.getLanguages();
        for (const locale of availableLanguages) {
            // For each language, we get the translations
            Logger.debug(locale, "Adding locale to available languages");
            await this.loadAndSubscribeLanguage(locale);
        }
        // Enable subscription to new languages being added while it is running
        await Client.subscribe(`/translations`, async (payload: Language) => {
            if (!this.languages.has(payload.language_id)) {
                Logger.debug(payload, "Adding new locale to available languages");
                await this.loadAndSubscribeLanguage(payload);
            }
        });
    }
    public async loadAndSubscribeLanguage(language: Language): Promise<void> {
        const translations = await Translations.Globals.getTranslations(language.language_id);
        this.languages.set(language.language_id, translations);
        await this.subscribe(language.language_id);
    }
    public get(lang: string, key: string): string {
        const language = this.languages.get(lang);
        if (language) {
            const translation = language.get(key);
            if (translation) {
                return translation;
            }
        }
        if (lang !== this.defaultLocale) {
            return this.get(this.defaultLocale, key);
        }
        return key;
    }
    public async subscribe(locale: string): Promise<void> {
        Logger.debug({locale}, "Subscribing to translations of the given locale.");
        await Client.subscribe(`/translations/${locale}`, (payload: Translation) => {
            Logger.debug(payload, "Received translation event");
            // We get the language and verify if it still exists
            const language = this.languages.get(locale);
            if (!language) {
                return;
            }
            if (!payload.key) {
                return;
            }
            if (!payload.value) {
                // Value is empty, key is to be deleted
                language.delete(payload.key);
            } else {
                language.set(payload.key, payload.value);
            }
        });
    }
}

export const singleton = new TranslationsManager();
export default {
    singleton,
};
